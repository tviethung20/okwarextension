package lta.servlet.transaction;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.servlet.Response;
import lta.base.GlobalNftMapper;
import lta.base.GlobalNftTransaction;
import lta.data.NftMapperData;
import lta.data.NftTransactionData;
import lta.servlet.token.HeroInformation;

import java.io.IOException;

public class SellingHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
        JsonNode node = mapper.readTree(body.get("body").textValue());

        String tokenId = node.get("tokenId").toString();
        String walletAddress = node.get("seller").textValue();
        String startingPrice = node.get("startingPrice").toString();
        String endingPrice = node.get("endingPrice").toString();
        String duration = node.get("duration").toString();
        String startedAt = node.get("startedAt").toString();

        return sellingToken(tokenId, walletAddress, startingPrice, endingPrice, duration, startedAt);
    }

    Object sellingToken(String token, String walletAddress, String startingPrice, String endingPrice, String durationIn, String startedIn) {
        try {
            double startPrice = Double.parseDouble(startingPrice);
            double endPrice = Double.parseDouble(endingPrice);
            Long duration = Long.parseLong(durationIn);
            Long startAt = Long.parseLong(startedIn);

            if (token == null || token.isEmpty() || walletAddress == null || walletAddress.isEmpty()) {
                return sendErrorResponse("{}", "Parameter isn't valid");
            }

            int tokenId = Integer.parseInt(token);

            if (!GlobalNftMapper.nftMapperModel.checkNftTokenIdExist(tokenId)) {
                return sendErrorResponse("{}", "tokenID isn't exists");
            }

            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);
            nftMapperData.setOnAuction(true);
            GlobalNftMapper.nftMapperModel.updateData(nftMapperData);

            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            NftTransactionData transactionData;
            if (GlobalNftTransaction.nftTransactionModel.checkNftTokenIdExist(tokenId, "PENDING")) {
                transactionData = GlobalNftTransaction.nftTransactionModel.getNftTokenId(tokenId, "PENDING");
                transactionData.setDuration(duration);
                transactionData.setStartedAt(startAt);
                transactionData.setStartingPrice(startPrice);
                transactionData.setEndingPrice(endPrice);
                GlobalNftTransaction.nftTransactionModel.updateData(transactionData);
            } else {
                transactionData = new NftTransactionData(tokenId, startPrice, endPrice, startAt, duration, walletAddress, "PENDING");
                GlobalNftTransaction.nftTransactionModel.insertData(transactionData);
            }
            objectNode.put("isOnAuction", true);
            objectNode.put("startAt", transactionData.getStartedAt());
            objectNode.put("duration", transactionData.getDuration());
            objectNode.put("startPrice", transactionData.getStartingPrice());
            objectNode.put("endPrice", transactionData.getEndingPrice());
            objectNode.put("seller", transactionData.getSeller());
            HeroInformation.updateHeroWeb(tokenId);
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }

}
