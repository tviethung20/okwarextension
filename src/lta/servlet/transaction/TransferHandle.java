package lta.servlet.transaction;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lta.base.GlobalVar;
import lta.base.fighter.FighterGlobalVar;
import lta.data.NftMapperData;
import lta.fighter.data.FighterData;
import lta.nft.models.NFTUserModel;
import lta.servlet.Response;
import lta.base.GlobalNftMapper;
import lta.data.UserData;
import lta.servlet.token.HeroInformation;

import java.io.IOException;

public class TransferHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
        JsonNode node = mapper.readTree(body.get("body").textValue());

        String tokenId = node.get("tokenId").toString();
        String walletAddress = node.get("address").textValue();

        return transferToken(tokenId, walletAddress);
    }

    Object transferToken(String token, String walletAddress) {
        try {
            checkParameter(token);
            checkParameter(walletAddress);

            int tokenId = Integer.parseInt(token);
            if(checkAddress(walletAddress) == false) {
                return sendErrorResponse("{}", "Address isn't valid");
            }

            UserData userData = ((NFTUserModel) GlobalVar.userModel).findUser(walletAddress);

            if (!GlobalNftMapper.nftMapperModel.checkNftTokenIdExist(tokenId)) {
                return sendErrorResponse("{}", "tokenID isn't exists");
            }

            //Update fighter for new user
            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);
            FighterData fighterData = FighterGlobalVar.fighterModel.getData(nftMapperData.getGameId());
            fighterData.ownId = walletAddress;
            FighterGlobalVar.fighterModel.updateData(fighterData);

            GlobalNftMapper.nftMapperModel.transferToken(tokenId, walletAddress);
            HeroInformation.updateHeroWeb(tokenId);
            return sendSuccessResponse("{}", "Transfer to " + walletAddress + " Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
