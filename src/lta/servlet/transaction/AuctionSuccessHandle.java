package lta.servlet.transaction;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lta.servlet.Response;
import lta.base.GlobalNftMapper;
import lta.base.GlobalNftTransaction;
import lta.data.NftMapperData;
import lta.data.NftTransactionData;
import lta.servlet.token.HeroInformation;

import java.io.IOException;

public class AuctionSuccessHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
        JsonNode node = mapper.readTree(body.get("body").textValue());

        String tokenId = node.get("tokenId").toString();
        String totalPrice = node.get("totalPrice").toString();

        return auctionSuccessToken(tokenId, totalPrice);
    }

    Object auctionSuccessToken(String token, String totalPrice) {
        try {
            checkParameter(token);
            checkParameter(totalPrice);

            double price = Double.parseDouble(totalPrice);
            int tokenId = Integer.parseInt(token);

            /*if (!GlobalNftTransaction.nftTransactionModel.checkNftTokenIdExist(tokenId, "PENDING")) {
                return sendErrorResponse("{}", "tokenID doesn't selling");
            }*/

            if (!GlobalNftMapper.nftMapperModel.checkNftTokenIdExist(tokenId)) {
                return sendErrorResponse("{}", "tokenID isn't exists");
            }

            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);
            nftMapperData.setOnAuction(false);
            nftMapperData.setLastPrice(price);
            GlobalNftMapper.nftMapperModel.updateData(nftMapperData);

            NftTransactionData transactionData = GlobalNftTransaction.nftTransactionModel.getNftTokenId(tokenId, "PENDING");
            if(transactionData != null) {
                transactionData.setStatus("SUCCESS");
                GlobalNftTransaction.nftTransactionModel.updateData(transactionData);
            }
            HeroInformation.updateHeroWeb(tokenId);
            return sendSuccessResponse("{}", "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
