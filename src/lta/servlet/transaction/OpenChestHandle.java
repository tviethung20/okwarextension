package lta.servlet.transaction;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.nft.models.NFTUserModel;
import lta.servlet.Response;
import lta.base.GlobalNftMapper;
import lta.data.NftMapperData;
import lta.servlet.token.HeroInformation;

import java.io.IOException;

public class OpenChestHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
//        JsonNode node = mapper.readTree(body.get("body").textValue());
        String authorization = body.get(AUTHORIZATION).textValue();
//        String tokenId = node.get("tokenId").toString();

        return openAChest(authorization);
    }

    Object openAChest(String authorization) {
        try {
            checkParameter(authorization);

            String walletAddress = NFTUserModel.getAddressFromSignature(authorization);

//            int tokenId = Integer.parseInt(token);

            if(checkAddress(walletAddress) == false) {
                return sendErrorResponse("{}", "Address isn't valid");
            }

            /*if (!GlobalNftMapper.nftMapperModel.checkNftTokenIdExist(tokenId)) {
                return sendErrorResponse("{}", "tokenID isn't exists");
            }*/

            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.getEggToken(walletAddress, true);

            if(nftMapperData == null) {
                return sendErrorResponse("{}", "Address hasn't capsule");
            }

            nftMapperData.setEgg(false);
            GlobalNftMapper.nftMapperModel.updateData(nftMapperData);

            HeroInformation.updateHeroWeb(nftMapperData.getTokenId());
            ObjectNode objectNode = HeroInformation.getHeroInformation(nftMapperData.getTokenId());
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
