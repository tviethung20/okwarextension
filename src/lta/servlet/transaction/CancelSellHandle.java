package lta.servlet.transaction;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.servlet.Response;
import lta.base.GlobalNftMapper;
import lta.base.GlobalNftTransaction;
import lta.data.NftMapperData;
import lta.data.NftTransactionData;
import lta.servlet.token.HeroInformation;

import java.io.IOException;

public class CancelSellHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
        JsonNode node = mapper.readTree(body.get("body").textValue());

        String tokenId = node.get("tokenId").toString();

        return cancelSellToken(tokenId);
    }

    Object cancelSellToken(String token) {
        try {
            checkParameter(token);

            int tokenId = Integer.parseInt(token);

            if (!GlobalNftTransaction.nftTransactionModel.checkNftTokenIdExist(tokenId, "PENDING")) {
                return sendErrorResponse("{}", "Transaction isn't exists");
            }

            if (!GlobalNftMapper.nftMapperModel.checkNftTokenIdExist(tokenId)) {
                return sendErrorResponse("{}", "tokenID isn't exists");
            }

            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);
            nftMapperData.setOnAuction(false);
            GlobalNftMapper.nftMapperModel.updateData(nftMapperData);

            NftTransactionData transactionData = GlobalNftTransaction.nftTransactionModel.getNftTokenId(tokenId, "PENDING");
            transactionData.setStatus("CANCELLED");
            GlobalNftTransaction.nftTransactionModel.updateData(transactionData);
            HeroInformation.updateHeroWeb(tokenId);
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("tokenId", transactionData.getTokenId());
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
