package lta.servlet.transaction;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lta.base.GlobalVar;
import lta.nft.models.NFTUserModel;
import lta.servlet.Response;
import lta.base.GlobalNftTransaction;
import lta.data.NftPointTransactionData;
import lta.data.UserData;

import java.io.IOException;

public class WithdrawPointHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
        JsonNode node = mapper.readTree(body.get("body").textValue());

        String point = node.get("point").toString();
        String signature = node.get("signature").textValue();
        String walletAddress = node.get("address").textValue();

        return withdrawPoint(signature, walletAddress, point);
    }

    Object withdrawPoint(String signature, String walletAddress, String points) {
        try {
            if (signature == null || signature.isEmpty() || walletAddress == null || walletAddress.isEmpty()) {
                return sendErrorResponse("{}", "Parameter isn't valid");
            }
            int point = Integer.parseInt(points);

            if(checkAddress(walletAddress) == false) {
                return sendErrorResponse("{}", "Address isn't valid");
            }

            UserData userData = ((NFTUserModel) GlobalVar.userModel).findUser(walletAddress);

            if(point == 0 || point > userData.totalDiamond) {
                return sendErrorResponse("{}", "insufficient_funds");
            }
            if (GlobalNftTransaction.nftPointTransactionModel.checkSignatureExist(signature)) {
                return sendErrorResponse("{}", "signature_duplicated");
            }

            NftPointTransactionData nftPointTransactionData = new NftPointTransactionData(signature, point, walletAddress);
            GlobalNftTransaction.nftPointTransactionModel.insertData(nftPointTransactionData);

            userData.totalDiamond -= point;
            GlobalVar.userModel.updateData(userData);

            nftPointTransactionData.setStatus("SUCCESS");
            GlobalNftTransaction.nftPointTransactionModel.updateData(nftPointTransactionData);

            return sendSuccessResponse("{}", "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
