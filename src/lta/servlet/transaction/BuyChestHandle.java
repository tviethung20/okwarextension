package lta.servlet.transaction;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.base.GlobalVar;
import lta.nft.models.NFTUserModel;
import lta.okwar.OkWarGlobalVar;
import lta.okwar.data.HeroData;
import lta.servlet.Response;
import lta.base.GlobalNftMapper;
import lta.data.NftMapperData;
import lta.data.UserData;
import lta.servlet.token.HeroInformation;
import lta.utils.GachaUtils;

import java.io.IOException;

public class BuyChestHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
        JsonNode node = mapper.readTree(body.get("body").textValue());

        String tokenId = node.get("tokenId").toString();
        String walletAddress = node.get("address").textValue();

        return createAChest(tokenId, walletAddress.toLowerCase());
    }

    Object createAChest(String token, String walletAddress) {
        try {
            checkParameter(token);
            checkParameter(walletAddress);

            int tokenId = Integer.parseInt(token);

            if(checkAddress(walletAddress) == false) {
                return sendErrorResponse("{}", "Address isn't valid");
            }

            UserData userData = ((NFTUserModel) GlobalVar.userModel).findUser(walletAddress);

            if (GlobalNftMapper.nftMapperModel.checkNftTokenIdExist(tokenId)) {
                return sendErrorResponse("{}", "tokenID already exists");
            }
            String heroConfigId = GachaUtils.gachaDataWithId("Gacha01");
            HeroData heroData = OkWarGlobalVar.heroModel.createHero("HeroConfig", heroConfigId, walletAddress);

            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.createNftData(tokenId, heroData.getId(), walletAddress, "Hero");

            HeroInformation.updateHeroWeb(tokenId);

            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("itemId", heroData.baseId);
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
