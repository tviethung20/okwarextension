package lta.servlet.transaction;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lta.base.GlobalVar;
import lta.nft.models.NFTUserModel;
import lta.servlet.Response;
import lta.base.GlobalNftTransaction;
import lta.data.NftPointTransactionData;
import lta.data.UserData;

import java.io.IOException;

public class DepositPointHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
        JsonNode node = mapper.readTree(body.get("body").textValue());

        String point = node.get("point").toString();
        String walletAddress = node.get("address").textValue();
        String txHash = node.get("txHash").textValue();

        return withdrawPoint(point, walletAddress.toLowerCase(), txHash);
    }

    Object withdrawPoint(String points, String walletAddress, String txHashS) {
        try {
            checkParameter(points);
            checkParameter(walletAddress);
            checkParameter(txHashS);

            int point = Integer.parseInt(points);

            if(checkAddress(walletAddress) == false) {
                return sendErrorResponse("{}", "Address isn't valid");
            }

            UserData userData = ((NFTUserModel) GlobalVar.userModel).findUser(walletAddress);

            if(GlobalNftTransaction.nftPointTransactionModel.checkTxHashExist(txHashS)) {
                return sendErrorResponse("{}", "txHash_duplicated");
            }

            NftPointTransactionData nftPointTransactionData = new NftPointTransactionData(point, walletAddress, txHashS);
            GlobalNftTransaction.nftPointTransactionModel.insertData(nftPointTransactionData);

            userData.totalDiamond += point;
            GlobalVar.userModel.updateData(userData);

            nftPointTransactionData.setStatus("SUCCESS");
            GlobalNftTransaction.nftPointTransactionModel.updateData(nftPointTransactionData);

            return sendSuccessResponse("{}", "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
