package lta.servlet;

public class ServletResponse {
    private static final int SUCCESS = 200;
    private static final int ERROR = 400;

    public int status;
    public String data;
    public String message;

    public ServletResponse(int status, String data, String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    @Override
    public String toString() {
        return "{" +
                "\"status\":\"" + status + "\"" +
                ",\"data\":" + data +
                ", \"message\":\"" + message + "\"" +
                "}";
    }

}
