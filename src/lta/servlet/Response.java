package lta.servlet;

import java.util.regex.Pattern;

public class Response {
    public static String AUTHORIZATION = "authorization";
    static Pattern pattern = Pattern.compile("^0x[a-fA-F0-9]{40}$");
    public Object sendSuccessResponse (String data, String message) {
        ServletResponse servletResponse = new ServletResponse(200, data, message);
        return servletResponse.toString();
    }

    public Object sendErrorResponse (String data, String message) {
        ServletResponse servletResponse = new ServletResponse(400, data, message);
        return servletResponse.toString();
    }

    public Object sendServletResponse (int status, String data, String message) {
        ServletResponse servletResponse = new ServletResponse(status, data, message);
        return servletResponse.toString();
    }

    public boolean checkAddress(String address) {
        return pattern.matcher(address).matches();
    }

    public void checkParameter(String param) throws Exception {
        if (param == null || param.isEmpty()) {
            throw new Exception("Parameter isn't valid");
        }
    }

}
