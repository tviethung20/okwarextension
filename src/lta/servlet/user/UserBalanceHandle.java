package lta.servlet.user;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.base.GlobalNftMapper;
import lta.base.GlobalVar;
import lta.data.NftMapperData;
import lta.data.UserData;
import lta.nft.models.NFTUserModel;
import lta.servlet.Response;
import org.bson.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserBalanceHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode data = mapper.readTree(params.toString());
        String authorization = data.get(AUTHORIZATION).textValue();

        return getUserBalance(authorization);
    }

    Object getUserBalance(String authorization) {
        try {
            checkParameter(authorization);

            String walletAddress = NFTUserModel.getAddressFromSignature(authorization);

            if(checkAddress(walletAddress) == false) {
                return sendErrorResponse("{}", "Address isn't valid");
            }

            UserData userData = ((NFTUserModel)GlobalVar.userModel).findUser(walletAddress);

            List<NftMapperData> userNFTData = new ArrayList<>();
            try {
                userNFTData = GlobalNftMapper.nftMapperModel.selectAllData(NftMapperData.class, new Document("walletAddress", walletAddress).append("isEgg", true));
            }catch (Exception e) {
                e.printStackTrace();
            }
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("point", userData.totalDiamond);
            objectNode.put("eggBalance", userNFTData != null ? userNFTData.size() : 0);
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
