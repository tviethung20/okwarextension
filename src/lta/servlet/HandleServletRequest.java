package lta.servlet;

import lta.servlet.hero.LockHeroHandle;
import lta.servlet.hero.UnLockHeroHandle;
import lta.servlet.key.ServletKey;
import lta.servlet.token.InventoryTokenListHandle;
import lta.servlet.token.MarketTokenHandle;
import lta.servlet.token.TokenIdsInformationHandle;
import lta.servlet.token.TokenInformationHandle;
import lta.servlet.transaction.*;
import lta.servlet.user.UserBalanceHandle;

public class HandleServletRequest extends Response {
    public Object handleServletRequest(String command, Object object) {
        try {
        switch (command) {
            case ServletKey.BUY_CHEST_CMD:
                BuyChestHandle buyChestHandle = new BuyChestHandle();
                return buyChestHandle.handler(object);
            case ServletKey.OPEN_CHEST_CMD:
                OpenChestHandle openChestHandle = new OpenChestHandle();
                return openChestHandle.handler(object);
            case ServletKey.TRANSFER_TOKEN_CMD:
                TransferHandle transferHandle = new TransferHandle();
                return transferHandle.handler(object);
            case ServletKey.SELLING_TOKEN_CMD:
                SellingHandle sellingHandle = new SellingHandle();
                return sellingHandle.handler(object);
            case ServletKey.CANCEL_SELLING_TOKEN_CMD:
                CancelSellHandle cancelSellHandle = new CancelSellHandle();
                return cancelSellHandle.handler(object);
            case ServletKey.AUCTION_SUCCESS_CMD:
                AuctionSuccessHandle auctionSuccessHandle = new AuctionSuccessHandle();
                return auctionSuccessHandle.handler(object);
            case ServletKey.GET_USER_INFORMATION_CMD:
                UserBalanceHandle userBalanceHandle = new UserBalanceHandle();
                return userBalanceHandle.handler(object);
            case ServletKey.LOCK_HERO_CMD:
                LockHeroHandle lockHeroHandle = new LockHeroHandle();
                return lockHeroHandle.handler(object);
            case ServletKey.UNLOCK_HERO_CMD:
                UnLockHeroHandle unLockHeroHandle = new UnLockHeroHandle();
                return unLockHeroHandle.handler(object);
            case ServletKey.GET_TOKEN_DETAIL_CMD:
                TokenInformationHandle tokenInformationHandle = new TokenInformationHandle();
                return tokenInformationHandle.handler(object);
            case ServletKey.GET_INVENTORY_CMD:
                InventoryTokenListHandle inventoryTokenListHandle = new InventoryTokenListHandle();
                return inventoryTokenListHandle.handler(object);
            case ServletKey.GET_TOKEN_INFORMATION_LIST_CMD:
                MarketTokenHandle marketTokenHandle = new MarketTokenHandle();
                return marketTokenHandle.handler(object);
            case ServletKey.REDUCE_POINT_CMD:
                WithdrawPointHandle withdrawPointHandle = new WithdrawPointHandle();
                return withdrawPointHandle.handler(object);
            case ServletKey.LIST_TOKEN_IDS_INFORMATION_CMD:
                TokenIdsInformationHandle tokenIdsInformationHandle = new TokenIdsInformationHandle();
                return tokenIdsInformationHandle.handler(object);
            case ServletKey.BUY_POINT_CMD:
                DepositPointHandle depositPointHandle = new DepositPointHandle();
                return depositPointHandle.handler(object);
            /*
            case ServletKey.REDUCE_POINT_CMD:
                break;
            case ServletKey.CHECK_FUSION_HERO_CMD:
                break;
            case ServletKey.UPGRADE_HERO_CMD:
                break;*/
        }
        return sendErrorResponse("{}", "Request isn't exist");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", "Exception: " + e.getMessage());
        }
    }
}
