package lta.servlet.token;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.client.model.Aggregates;
import lta.servlet.Response;
import lta.base.GlobalNftTransaction;
import lta.data.NftHeroWebData;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;

public class MarketTokenHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();
    private static final String LATEST = "LATEST";
    private static final String PRICE_ASC = "PRICE_ASC";
//    private static final String PRICE_DESC = "PRICE_DESC";
    public Object handler(Object params) throws IOException {
        JsonNode data = mapper.readTree(params.toString());
//        String authorization = data.get(AUTHORIZATION).textValue();
        String page = "1";
        String page_size = "60";
        String isEgg = "false";
        String is_on_auction = "true";
        String gen = null;
        String rarity = null;
        String race = null;
        String level_min = "0";
        String level_max = "100";
        String sort = LATEST;

        if(data.has("page")) {
            page = data.get("page").textValue();
        }

        if(data.has("page_size")) {
            page_size = data.get("page_size").textValue();
        }

        if(data.has("isEgg")) {
            isEgg = data.get("isEgg").textValue();
        }

        if(data.has("is_on_auction")) {
            is_on_auction = data.get("is_on_auction").textValue();
        }

        if(data.has("gen")) {
            gen = data.get("gen").textValue();
        }

        if(data.has("rarity")) {
            rarity = data.get("rarity").textValue();
        }

        if(data.has("race")) {
            race = data.get("race").textValue();
        }

        if(data.has("level_min")) {
            level_min = data.get("level_min").textValue();
        }

        if(data.has("level_max")) {
            level_max = data.get("level_max").textValue();
        }


        if(data.has("sort")) {
            sort = data.get("sort").textValue();
        }

        return getTokenListInformation(page, page_size, isEgg, is_on_auction, gen, rarity, race, level_min, level_max, sort);
    }

    Object getTokenListInformation(String pages, String page_sizes, String isEggs, String isOnAuctions, String genQuery, String rarityQuery, String raceQuery, String level_min, String level_max, String sortQuery) {
        try {
            /*if (authorization == null || authorization.isEmpty() ) {
                return sendErrorResponse("{}", "Parameter isn't valid");
            }

            String walletAddress = NFTUserModel.getAddressFromSignature(authorization);

            if(checkAddress(walletAddress) == false) {
                return sendErrorResponse("{}", "Address isn't valid");
            }*/

            int page = Integer.parseInt(pages);
            int pageSize = Integer.parseInt(page_sizes);
            int levelMin = Integer.parseInt(level_min);
            int levelMax = Integer.parseInt(level_max);
            if(pageSize >= 60) pageSize = 60;
            boolean isEgg = Boolean.parseBoolean(isEggs);
            boolean isOnAuction = Boolean.parseBoolean(isOnAuctions);

            List<Bson> conditions = new ArrayList<>();
//            Bson match = match(or(eq("heroConfigId","Hero01"), eq("heroConfigId","Hero02")));
            Bson eggCondition = match(eq("isEgg", isEgg));
            Bson auctionCondition = match(eq("isOnAuction", isOnAuction));
            Bson limit = Aggregates.limit(pageSize);
            Bson skip = Aggregates.skip(pageSize * (page - 1));
            Bson range = match(and(gte("level", levelMin), lte("level", levelMax)));
//            coun
            conditions.add(eggCondition);
            conditions.add(auctionCondition);
            conditions.add(range);
            addBsonCondition(genQuery, "gen", conditions);
            addBsonCondition(rarityQuery, "rarity", conditions);
            addBsonCondition(raceQuery, "race", conditions);
            /*if(sortQuery.equals(PRICE_DESC)) {
                conditions.add(sort(descending("startingPrice")));
            }else */
            if(sortQuery.equals(PRICE_ASC)) {
                conditions.add(sort(ascending("startingPrice")));
            } else {
                conditions.add(sort(descending("startedAt")));
            }
            List<Bson> countConditions = new ArrayList<>(conditions);
            conditions.add(skip);
            conditions.add(limit);

//            Document document = new Document("owner", walletAddress).append("isEgg", isEgg).append("isOnAuction", isOnAuction);
            List<NftHeroWebData> nftHeroWebData = GlobalNftTransaction.nftHeroWebModel.filterAllData(NftHeroWebData.class, conditions);
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("total", GlobalNftTransaction.nftHeroWebModel.getFilterCount(NftHeroWebData.class, countConditions));
            objectNode.put("page", page);
            JsonNode objectNode1 = (JsonNode)mapper.valueToTree(nftHeroWebData);
            objectNode.putPOJO("data", objectNode1);
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("", e.getMessage());
        }
    }

    void addBsonCondition(String params, String key, List<Bson> bsons) {
        if(params == null || params.isEmpty()) return;

        String[] paramArr = params.split(",");
        //Bson match = match(or(eq("heroConfigId","Hero01"), eq("heroConfigId","Hero02")));
        List<Bson> conditions = new ArrayList<>();
        for(String param : paramArr) {
            Bson temp = eq(key, param);
            conditions.add(temp);
        }
        Bson condition = match(or(conditions));
        bsons.add(condition);
    }

    /*public static void main(String[] args) {
        List<Bson> a = new ArrayList<>();
        String param = "a,b,c";
        String key = "hero";
        addBsonCondition(param, key, a);
        System.out.println(a.size());
    }*/
}
