package lta.servlet.token;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.servlet.Response;
import lta.base.GlobalNftMapper;
import lta.data.NftMapperData;

import java.io.IOException;

public class TokenInformationHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode data = mapper.readTree(params.toString());
        //String authorization = data.get(AUTHORIZATION).textValue();
        String tokenId = data.get("token_id").textValue();
        return getTokenInformation(tokenId);
    }

    Object getTokenInformation(String token) {
        try {
            checkParameter(token);
            //String walletAddress = NFTUserModel.getAddressFromSignature(authorization);

            int tokenId = Integer.parseInt(token);

            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);
            String response = "";
            if(nftMapperData.getType().equals("Hero")) {
                /*HeroInformation heroInformation = new HeroInformation(tokenId);
                if(heroInformation == null) {
                    return sendErrorResponse("", "Cannot find hero information");
                }
                response = mapper.writeValueAsString(heroInformation);*/
                ObjectNode objectNode = HeroInformation.getHeroInformation(tokenId);
                return sendSuccessResponse(objectNode.toString(), "Success");
            }
            return sendSuccessResponse(response, "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
