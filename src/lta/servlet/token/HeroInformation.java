package lta.servlet.token;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.base.GlobalHeroConfig;
import lta.base.GlobalNftMapper;
import lta.base.GlobalNftTransaction;
import lta.base.entity.EntityInformationGlobalVar;
import lta.base.fighter.FighterGlobalVar;
import lta.data.*;
import lta.okwar.OkWarGlobalVar;
import lta.okwar.data.BaseHero;
import lta.okwar.data.HeroData;

public class HeroInformation {
    public String heroId;
    public String name;
    public int level;
    public String rarity;
    public String gen;
    public String race;
    public boolean isOnAuction;
    public boolean isEgg;
    public double startingPrice;
    public double endingPrice;
    public long duration;
    public long startedAt;
    public HeroStatsData heroStatsData;

    /*public HeroInformation(int tokenId) {
        try {
            HeroData heroData = GlobalHeroes.heroesModel.getHeroWithHeroId(tokenId);
            HeroConfigData heroConfigData = GlobalHeroConfig.heroConfigModel.getHeroConfig(heroData.heroConfigId);
            heroId = heroData.heroConfigId;
            level = heroData.level;
            name = heroConfigData.heroName;
            rarity = heroConfigData.rarity;
            gen = heroConfigData.heroGen;
            race = heroConfigData.heroRace;
            heroStatsData = heroConfigData.heroStats.length < level ? heroConfigData.heroStats[heroConfigData.heroStats.length - 1] : heroConfigData.heroStats[level - 1];

            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);
            if(nftMapperData != null) {
                isOnAuction = nftMapperData.isOnAuction();
                isEgg = nftMapperData.isEgg();
            }

            if(isOnAuction) {
                NftTransactionData nftTransactionData = GlobalNftTransaction.nftTransactionModel.getNftTokenId(tokenId);
                startingPrice = nftTransactionData.getStartingPrice();
                endingPrice = nftTransactionData.getEndingPrice();
                duration = nftTransactionData.getDuration();
                startedAt = nftTransactionData.getStartedAt();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static void updateHeroWeb(int tokenId) {
        try {
            NftMapperData nft = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);
            NftHeroWebData heroWebData = null;
            boolean isNew = true;
            try {
                heroWebData = GlobalNftTransaction.nftHeroWebModel.getHeroData(tokenId);
                isNew = false;
            } catch (Exception e) {
            }
            if (heroWebData == null) {
                heroWebData = new NftHeroWebData();
            }
            heroWebData.setIsEgg(nft.isEgg());
            heroWebData.setIsOnAuction(nft.isOnAuction());
            heroWebData.setTokenId(nft.getTokenId());
            heroWebData.setOwner(nft.getWalletAddress());
            heroWebData.isLock = nft.isLock();
            HeroData heroData = OkWarGlobalVar.heroModel.getHeroById(nft.getGameId());
            BaseHero heroConfigData = (BaseHero) EntityInformationGlobalVar.map_Type_BaseEntityData.get("HeroConfig").get(heroData.baseId);
            //HeroConfigData heroConfigData = GlobalHeroConfig.heroConfigModel.getHeroConfig(heroData.baseId);
            heroWebData.setHeroId(heroData.baseId);
            heroWebData.setLevel(heroData.level);
            heroWebData.setName(heroConfigData.name);
            heroWebData.setRarity(heroConfigData.rarity);
            heroWebData.setGen(heroConfigData.heroGen);
            heroWebData.setRace(heroConfigData.heroRace);

            if (nft.isOnAuction()) {
                NftTransactionData nftTransactionData = GlobalNftTransaction.nftTransactionModel.getNftTokenId(nft.getTokenId(), "PENDING");

                heroWebData.setStartingPrice(nftTransactionData.getStartingPrice());
                heroWebData.setEndingPrice(nftTransactionData.getEndingPrice());
                heroWebData.setDuration(nftTransactionData.getDuration());
                heroWebData.setStartedAt(nftTransactionData.getStartedAt());
            }
            if (isNew) {
                GlobalNftTransaction.nftHeroWebModel.insertData(heroWebData);
            } else {
                GlobalNftTransaction.nftHeroWebModel.updateData(heroWebData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ObjectNode getHeroInformation(int tokenId) {
        ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
        try {
            boolean isEgg = true;
            boolean isOnAuction = false;
            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);
            if (nftMapperData != null) {
                objectNode.put("isOnAuction", nftMapperData.isOnAuction());
                objectNode.put("tokenId", nftMapperData.getTokenId());
                objectNode.put("isEgg", nftMapperData.isEgg());
                objectNode.put("owner", nftMapperData.getWalletAddress());
                objectNode.put("isLock", nftMapperData.isLock());
            }

//            if(!isEgg) {
            HeroData heroData = OkWarGlobalVar.heroModel.getHeroById(nftMapperData.getGameId());
            BaseHero heroConfigData = (BaseHero) EntityInformationGlobalVar.map_Type_BaseEntityData.get("HeroConfig").get(heroData.baseId);
            int level = heroData.level;
            objectNode.put("heroId", heroData.baseId);
            objectNode.put("level", heroData.level);
            objectNode.put("name", heroConfigData.name);
            objectNode.put("rarity", heroConfigData.rarity);
            objectNode.put("gen", heroConfigData.heroGen);
            objectNode.put("race", heroConfigData.heroRace);
            objectNode.put("description", "");
            //ObjectMapper objectMapper = new ObjectMapper();
            //HeroStatsData heroStatsData = heroConfigData.heroStats.length < level ? heroConfigData.heroStats[heroConfigData.heroStats.length - 1] : heroConfigData.heroStats[level - 1];
//                objectNode.put("heroStatsData", objectMapper.writeValueAsString(heroStatsData));
            //objectNode.putPOJO("heroStats", objectMapper.writeValueAsString(heroStatsData));
//            }
            if (nftMapperData.isOnAuction()) {
                NftTransactionData nftTransactionData = GlobalNftTransaction.nftTransactionModel.getNftTokenId(tokenId, "PENDING");
                objectNode.put("startingPrice", nftTransactionData.getStartingPrice());
                objectNode.put("endingPrice", nftTransactionData.getEndingPrice());
                objectNode.put("duration", nftTransactionData.getDuration());
                objectNode.put("startedAt", nftTransactionData.getStartedAt());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return objectNode;
    }
}
