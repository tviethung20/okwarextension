package lta.servlet.token;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.nft.models.NFTUserModel;
import lta.servlet.Response;
import lta.base.GlobalNftTransaction;
import lta.data.NftHeroWebData;
import org.bson.Document;

import java.io.IOException;
import java.util.List;

public class InventoryTokenListHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode data = mapper.readTree(params.toString());
        String authorization = data.get(AUTHORIZATION).textValue();
        String page = "1";
        String page_size = "60";
        String isEgg = "true";
        String is_on_auction = "false";

        if(data.has("page")) {
            page = data.get("page").textValue();
        }

        if(data.has("page_size")) {
            page_size = data.get("page_size").textValue();
        }

        if(data.has("isEgg")) {
            isEgg = data.get("isEgg").textValue();
        }

        if(data.has("is_on_auction")) {
            is_on_auction = data.get("is_on_auction").textValue();
        }

        return getTokenListInformation(authorization, page, page_size, isEgg, is_on_auction);
    }

    Object getTokenListInformation(String authorization, String pages, String page_sizes, String isEggs, String isOnAuctions) {
        try {
            checkParameter(authorization);

            String walletAddress = NFTUserModel.getAddressFromSignature(authorization);

            if(checkAddress(walletAddress) == false) {
                return sendErrorResponse("{}", "Address isn't valid");
            }

            int page = Integer.parseInt(pages);
            int pageSize = Integer.parseInt(page_sizes);
            if(pageSize >= 60) pageSize = 60;
            boolean isEgg = Boolean.parseBoolean(isEggs);
            boolean isOnAuction = Boolean.parseBoolean(isOnAuctions);

            Document document = new Document("owner", walletAddress).append("isEgg", isEgg).append("isOnAuction", isOnAuction);
            List<NftHeroWebData> nftHeroWebData = GlobalNftTransaction.nftHeroWebModel.getAllHero(page, pageSize, document);
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("total", GlobalNftTransaction.nftHeroWebModel.getTotalRecord(document));
            objectNode.put("page", page);
            JsonNode objectNode1 = (JsonNode)mapper.valueToTree(nftHeroWebData);
            objectNode.putPOJO("data", objectNode1);
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("", e.getMessage());
        }
    }
}
