package lta.servlet.token;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.client.model.Aggregates;
import lta.servlet.Response;
import lta.base.GlobalNftTransaction;
import lta.data.NftHeroWebData;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.or;

public class TokenIdsInformationHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode data = mapper.readTree(params.toString());
        //String authorization = data.get(AUTHORIZATION).textValue();
        String tokenIds = data.get("token_ids").textValue();
        String page = "1";
        String page_size = "60";
        if(data.has("page")) {
            page = data.get("page").textValue();
        }

        if(data.has("page_size")) {
            page_size = data.get("page_size").textValue();
        }
        return getTokenInformation(tokenIds, page, page_size);
    }

    Object getTokenInformation(String tokenIds, String pages, String page_sizes) {
        try {
            checkParameter(tokenIds);

            int page = Integer.parseInt(pages);
            int pageSize = Integer.parseInt(page_sizes);
            List<Bson> conditions = new ArrayList<>();
            addBsonCondition(tokenIds, "tokenId", conditions);
            List<Bson> countConditions = new ArrayList<>(conditions);

            Bson limit = Aggregates.limit(pageSize);
            Bson skip = Aggregates.skip(pageSize * (page - 1));

            conditions.add(skip);
            conditions.add(limit);

            List<NftHeroWebData> nftHeroWebData = GlobalNftTransaction.nftHeroWebModel.filterAllData(NftHeroWebData.class, conditions);
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("total", GlobalNftTransaction.nftHeroWebModel.getFilterCount(NftHeroWebData.class, countConditions));
            objectNode.put("page", page);
            JsonNode objectNode1 = (JsonNode)mapper.valueToTree(nftHeroWebData);
            objectNode.putPOJO("data", objectNode1);
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }

    void addBsonCondition(String params, String key, List<Bson> bsons) {
        if(params == null || params.isEmpty()) return;

        String[] paramArr = params.split(",");
        //Bson match = match(or(eq("heroConfigId","Hero01"), eq("heroConfigId","Hero02")));
        List<Bson> conditions = new ArrayList<>();
        for(String param : paramArr) {
            Bson temp = eq(key, Integer.parseInt(param));
            conditions.add(temp);
        }
        Bson condition = match(or(conditions));
        bsons.add(condition);
    }
}
