package lta.servlet.hero;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lta.base.GlobalNftMapper;
import lta.data.NftMapperData;
import lta.servlet.Response;
import lta.servlet.token.HeroInformation;

import java.io.IOException;


public class UnLockHeroHandle extends Response {
    static ObjectMapper mapper = new ObjectMapper();

    public Object handler(Object params) throws IOException {
        JsonNode body = mapper.readTree(params.toString());
        JsonNode node = mapper.readTree(body.get("body").textValue());

        String tokenId = node.get("tokenId").toString();

        return unlockHero(tokenId);
    }

    Object unlockHero(String token) {
        try {
            checkParameter(token);

            int tokenId = Integer.parseInt(token);
            if (!GlobalNftMapper.nftMapperModel.checkNftTokenIdExist(tokenId)) {
                return sendErrorResponse("{}", "tokenID isn't exists");
            }

            NftMapperData nft = GlobalNftMapper.nftMapperModel.getNftTokenId(tokenId);

            nft.setLock(false);

            GlobalNftMapper.nftMapperModel.updateData(nft);
            HeroInformation.updateHeroWeb(tokenId);
            ObjectNode objectNode = new ObjectNode(JsonNodeFactory.instance);
            objectNode.put("tokenId", nft.getTokenId());
            objectNode.put("isLock", nft.isLock());
            return sendSuccessResponse(objectNode.toString(), "Success");
        } catch (Exception e) {
            e.printStackTrace();
            return sendErrorResponse("{}", e.getMessage());
        }
    }
}
