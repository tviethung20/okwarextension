package lta.servlet.key;

public class ServletKey {
    public static final String BUY_CHEST_CMD= "BuyChest";
    public static final String OPEN_CHEST_CMD= "OpenChest";
    public static final String GET_INVENTORY_CMD= "GetInventory";
    public static final String TRANSFER_TOKEN_CMD= "TransferToken";
    public static final String SELLING_TOKEN_CMD= "SellingToken";
    public static final String CANCEL_SELLING_TOKEN_CMD= "CancelSellingToken";
    public static final String AUCTION_SUCCESS_CMD= "AuctionSuccessToken";
    public static final String GET_USER_INFORMATION_CMD= "GetUserInformation";
    public static final String GET_TOKEN_INFORMATION_LIST_CMD= "GetTokenInformationList";
    public static final String GET_TOKEN_DETAIL_CMD= "GetTokenDetail";
    public static final String CHECK_FUSION_HERO_CMD= "CheckFusionHero";
    public static final String UPGRADE_HERO_CMD= "UpgradeHero";
    public static final String LOCK_HERO_CMD= "LockHero";
    public static final String UNLOCK_HERO_CMD= "UnlockHero";
    public static final String REDUCE_POINT_CMD= "ReducePoint";
    public static final String BUY_POINT_CMD= "BuyPoint";
    public static final String LIST_TOKEN_IDS_INFORMATION_CMD= "ListTokenIdsInformation";
}
