package lta.okwar.handle.reward;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import lta.base.GlobalVar;
import lta.base.entities.BaseUser;
import lta.base.item.entities.ItemEntity;
import lta.base.reward.data.RewardItemData;
import lta.base.reward.key.RewardKey;
import lta.okwar.entities.mapstate.ShibaMapStateEntity;
import lta.okwar.entities.reward.ShibaRewardEntity;
import lta.smartfox.error.ErrorException;

import java.util.List;

public class ShibaRewardHandle extends BaseClientRequestHandler {
    @Override
    public void handleClientRequest(User user, ISFSObject isfsObject) {
        String baseId = isfsObject.getUtfString(RewardKey.REWARD_ID);
        if (baseId == null || baseId.isEmpty()) new ErrorException(getParentExtension(), user, "Reward isn't exist!");
        BaseUser entity = GlobalVar.map_Users.get(user);
        ShibaRewardEntity shibaRewardEntity = (ShibaRewardEntity) entity.getEntity("Reward");

        //Send reward to client
        List<RewardItemData> listReward = shibaRewardEntity.generateReward(baseId);

        //Update Map State
        ShibaMapStateEntity shibaMapStateEntity = (ShibaMapStateEntity) entity.getEntity("MapState");
        shibaMapStateEntity.clearMap(baseId);

        //Update item in stogare
        if (listReward != null) {
            ItemEntity itemEntity = (ItemEntity) entity.getEntity("Items");
            for (RewardItemData reward : listReward) {
                itemEntity.addItem(reward.itemId, "ItemConfig", reward.amount, "Fragment", reward.type);
            }
            try {
                itemEntity.updateInfo("Fragment");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
