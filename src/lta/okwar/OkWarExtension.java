package lta.okwar;

//import com.smartfoxserver.v2.core.ISFSEvent;
//import com.smartfoxserver.v2.entities.User;
//import com.smartfoxserver.v2.entities.data.ISFSObject;
//import lta.GlobalGachaSystem;
//import lta.HandleServletRequest;
//import lta.base.BaseNFTExtension;
//import lta.base.GlobalHeroConfig;
//import lta.base.GlobalSkillConfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartfoxserver.v2.controllers.system.Logout;
import lta.GlobalGachaSystem;
import lta.base.*;
import lta.base.entity.BaseEntityUtils;
import lta.base.entity.EntityInformationGlobalVar;
import lta.base.entity.entities.BaseEntityInformation;
import lta.base.login.LogoutHandler;
import lta.base.model.RewardConfigModel;
import lta.base.reward.RewardGlobalVar;
import lta.base.reward.key.RewardKey;
import lta.data.*;
import lta.mongodb.MongoDBManager;
import lta.nft.BaseNFTExtension;
import lta.nft.login.NFTLoginHandler;
import lta.nft.models.NFTUserModel;
import lta.okwar.data.BaseHero;
import lta.okwar.data.HeroData;
import lta.okwar.data.reward.ShibaBaseReward;
import lta.okwar.handle.reward.ShibaRewardHandle;
import lta.okwar.items.OkWarBaseItem;
import lta.okwar.login.OkWarLoginSuccess;
import lta.okwar.model.HeroModel;
import lta.okwar.model.mapstate.ShibaMapStateModel;
import lta.servlet.HandleServletRequest;
import lta.servlet.token.HeroInformation;
import lta.utils.GachaUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OkWarExtension extends BaseNFTExtension<NFTLoginHandler, OkWarLoginSuccess, LogoutHandler> {
    @Override
    protected Class<OkWarLoginSuccess> getLoginSuccessHandler() {
        return OkWarLoginSuccess.class;
    }

    @Override
    public void init() {
        super.init();
        addRequestHandler(RewardKey.REQUEST_REWARD, ShibaRewardHandle.class);

        try {
            //Init hero config sync hero data for market
            try {
                Thread.sleep(1000);
                do {
                    OkWarGlobalVar.heroModel = new HeroModel();
                    OkWarGlobalVar.mapStateModel = new ShibaMapStateModel();
                    RewardConfigModel rewardConfigModel = new RewardConfigModel();
                    rewardConfigModel.mockData();
                    OkWarGlobalVar.heroModel.addBaseNameField();
                    //RewardGlobalVar.rewardModel.createReward("map_01", "RewardConfig");
                    //GlobalHeroConfig.heroConfigModel.mockData();

                    //GlobalGachaSystem.gachaSystemModel.mockData();
                } while(OkWarGlobalVar.heroModel == null || GlobalHeroConfig.heroConfigModel == null || GlobalGachaSystem.gachaSystemModel == null);
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            BaseEntityUtils.<BaseHero>InitBaseEntity("HeroConfig", BaseHero.class);
            BaseEntityUtils.<OkWarBaseItem>InitBaseEntity("BaseItems", OkWarBaseItem.class);
            BaseEntityUtils.<ShibaBaseReward>InitBaseEntity("RewardConfig", ShibaBaseReward.class);
//            HashMap<String, BaseEntityInformation> map_BaseItems = EntityInformationGlobalVar.map_Type_BaseEntityData.get("RewardConfig");
            //synchHeroWeb();
//            synchOldData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    @Override
//    public void init() {
//        super.init();
//        trace("-------Init oke war------------");
//        try {
//            Thread.sleep(1000);
//            GlobalSkillConfig.skillModel.mockData();
//            GlobalHeroConfig.heroConfigModel.mockData();
//            GlobalGachaSystem.gachaSystemModel.mockData();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    HandleServletRequest servletRequest = new HandleServletRequest();

    @Override
    public Object handleInternalMessage(String cmdName, Object params) {
//        trace("-------Servlet request: " + cmdName + params);
        Object object = servletRequest.handleServletRequest(cmdName, params);
//        trace("-------Servlet response: " + object);
        return object;
    }
//
//    @Override
//    public void handleClientRequest(String requestId, User sender, ISFSObject params) {
//        super.handleClientRequest(requestId, sender, params);
//    }
//
//    @Override
//    public void handleServerEvent(ISFSEvent event) throws Exception {
//        super.handleServerEvent(event);
//    }

    public void synchHeroWeb() throws Exception {
        List<NftMapperData> nftMapperDataList = GlobalNftMapper.nftMapperModel.selectAllData(NftMapperData.class);
        for (NftMapperData nft : nftMapperDataList) {
            NftHeroWebData heroWebData = null;
            boolean isNew = true;
            try {
                heroWebData = GlobalNftTransaction.nftHeroWebModel.getHeroData(nft.getTokenId());
                trace("Hero web: " + heroWebData.tokenId);
                isNew = false;
            } catch (Exception e) {
            }
            if (heroWebData == null) {
                heroWebData = new NftHeroWebData();
                trace("New: ");
            }
            heroWebData.setIsEgg(nft.isEgg());
            heroWebData.setTokenId(nft.getTokenId());
            heroWebData.setOwner(nft.getWalletAddress());
            heroWebData.isLock = nft.isLock();

            HeroData heroData = OkWarGlobalVar.heroModel.getHeroById(nft.getGameId());
            BaseHero heroConfigData = (BaseHero) EntityInformationGlobalVar.map_Type_BaseEntityData.get("HeroConfig").get(heroData.baseId);
            heroWebData.setHeroId(heroData.baseId);
            heroWebData.setLevel(heroData.level);
            heroWebData.setName(heroConfigData.name);
            heroWebData.setRarity(heroConfigData.rarity);
            heroWebData.setGen(heroConfigData.heroGen);
            heroWebData.setRace(heroConfigData.heroRace);

            if (nft.isOnAuction() || GlobalNftTransaction.nftTransactionModel.checkNftTokenIdExist(nft.getTokenId(), "PENDING")) {
                nft.setOnAuction(true);
                GlobalNftMapper.nftMapperModel.updateData(nft);

                NftTransactionData nftTransactionData = GlobalNftTransaction.nftTransactionModel.getNftTokenId(nft.getTokenId(), "PENDING");
                heroWebData.setStartingPrice(nftTransactionData.getStartingPrice());
                heroWebData.setEndingPrice(nftTransactionData.getEndingPrice());
                heroWebData.setDuration(nftTransactionData.getDuration());
                heroWebData.setStartedAt(nftTransactionData.getStartedAt());
            }

            heroWebData.setIsOnAuction(nft.isOnAuction());
            if (isNew) {
                GlobalNftTransaction.nftHeroWebModel.insertData(heroWebData);
            } else {
                GlobalNftTransaction.nftHeroWebModel.updateData(heroWebData);
            }
        }
    }

    /*public void synchOldData() throws Exception {
        List<OldData> list = new ArrayList<>();
        list.add(new OldData(1, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(3, "0xc657654e342332dce13044a67b483e4a74e02f5c", "Hero06"));
        list.add(new OldData(4, "0xc657654e342332dce13044a67b483e4a74e02f5c", "Hero01"));
        list.add(new OldData(5, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero01"));
        list.add(new OldData(6, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(7, "0xc038406b5ebe51423890d5f8e7bfb2dbc7396eb2", "Hero02"));
        list.add(new OldData(8, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(9, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero01"));
        list.add(new OldData(10, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(11, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero01"));
        list.add(new OldData(12, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(13, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero06"));
        list.add(new OldData(14, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero06"));
        list.add(new OldData(15, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero06"));
        list.add(new OldData(16, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(17, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(18, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(19, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero01"));
        list.add(new OldData(20, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero02"));
        list.add(new OldData(21, "0x5ebc410704b4fff44bac042383b22289b2a567dc", "Hero06"));

        for(OldData oldData : list) {
            UserData userData = ((NFTUserModel) GlobalVar.userModel).findUser(oldData.walletAddress);

            if (GlobalNftMapper.nftMapperModel.checkNftTokenIdExist(oldData.tokenId)) {
                continue;
            }
            String heroConfigId = GachaUtils.gachaDataWithIdAndBaseId("Gacha01", oldData.heroId);
            HeroData heroData = OkWarGlobalVar.heroModel.createHero("HeroConfig", heroConfigId, oldData.walletAddress);

            NftMapperData nftMapperData = GlobalNftMapper.nftMapperModel.createNftData(oldData.tokenId, heroData.getId(), oldData.walletAddress, "Hero");

            HeroInformation.updateHeroWeb(oldData.tokenId);
        }
    }*/

    public class OldData{
        public int tokenId;
        public String walletAddress;
        public String heroId;

        public OldData(int tokenId, String walletAddress, String heroId) {
            this.tokenId = tokenId;
            this.walletAddress = walletAddress;
            this.heroId = heroId;
        }

        public int getTokenId() {
            return tokenId;
        }

        public void setTokenId(int tokenId) {
            this.tokenId = tokenId;
        }

        public String getWalletAddress() {
            return walletAddress;
        }

        public void setWalletAddress(String walletAddress) {
            this.walletAddress = walletAddress;
        }

        public String getHeroId() {
            return heroId;
        }

        public void setHeroId(String heroId) {
            this.heroId = heroId;
        }
    }
}
