package lta.okwar.login;

import lta.base.entities.BaseUser;
import lta.base.login.LoginSuccessHandler;
import lta.data.UserData;
import lta.okwar.entities.OkWarEntity;

public class OkWarLoginSuccess extends LoginSuccessHandler {
    @Override
    protected BaseUser createNewUser() {
        return new OkWarEntity(this.getParentExtension());
    }
}
