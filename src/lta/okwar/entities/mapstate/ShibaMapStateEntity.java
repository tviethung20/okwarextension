package lta.okwar.entities.mapstate;

import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.SFSExtension;
import lta.base.GlobalVar;
import lta.base.entity.EntityInformationGlobalVar;
import lta.base.mapstate.data.MapStateData;
import lta.base.mapstate.entities.MapStateEntity;
import lta.okwar.OkWarGlobalVar;
import lta.okwar.data.mapstate.ShibaMapStateData;
import lta.okwar.data.reward.ShibaBaseReward;
import lta.okwar.entities.reward.ShibaRewardEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShibaMapStateEntity extends MapStateEntity<ShibaMapStateData> {
    Map<Integer, ShibaMapStateData> mapStateDataMap = new HashMap<>();

    public ShibaMapStateEntity(SFSExtension extension, String ownId) {
        super(extension, ownId, "MapCompleted");
        for (ShibaMapStateData mapData : map_BaseId_Data.values()) {
            mapStateDataMap.put(mapData.story, mapData);
        }
    }

    public void clearMap(String baseId) {
        try {
            ShibaBaseReward shibaBaseReward = (ShibaBaseReward) EntityInformationGlobalVar.map_Type_BaseEntityData.get("RewardConfig").get(baseId);
            if (mapStateDataMap.containsKey(shibaBaseReward.story)) {
                ShibaMapStateData shibaMapStateData = mapStateDataMap.get(shibaBaseReward.story);
                int tempCount = (shibaBaseReward.mission - shibaMapStateData.mission) + (shibaBaseReward.stage - shibaMapStateData.stage);
                if(tempCount > 0) {
                    String oldBaseId = shibaMapStateData.baseId;
                    shibaMapStateData.baseId = shibaBaseReward.baseId;
                    shibaMapStateData.mission = shibaBaseReward.mission;
                    shibaMapStateData.stage = shibaBaseReward.stage;
                    OkWarGlobalVar.mapStateModel.updateData(shibaMapStateData);
                    addNewData(shibaMapStateData, oldBaseId);
                    updateInfo();
                }
            } else {
                ShibaMapStateData mapStateData = OkWarGlobalVar.mapStateModel.createMapState(baseId, "RewardConfig", 1, "campaign", ownId);
                addNewData(mapStateData, "");
                updateInfo();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addNewData(ShibaMapStateData entityData, String oldBaseId) {
        if(this.map_BaseId_Data.containsKey(oldBaseId)) {
            this.map_BaseId_Data.remove(oldBaseId);
        }

        this.map_BaseId_Data.put(entityData.baseId, entityData);
        mapStateDataMap.put(entityData.story, entityData);
    }

    @Override
    protected List<ShibaMapStateData> getEntities(String s) throws Exception {
        return OkWarGlobalVar.mapStateModel.selectAllData(ShibaMapStateData.class);
    }

    @Override
    protected void AddInfo(SFSObject infoItem, MapStateData entity) {
        super.AddInfo(infoItem, entity);
        ShibaMapStateData shibaMapStateData = (ShibaMapStateData) entity;
        infoItem.putInt("story", shibaMapStateData.story);
        infoItem.putInt("mission", shibaMapStateData.mission);
        infoItem.putInt("stage", shibaMapStateData.stage);
    }
}
