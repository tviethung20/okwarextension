package lta.okwar.entities;

import com.smartfoxserver.v2.extensions.SFSExtension;
import lta.base.GlobalNftMapper;
import lta.base.entities.BaseUser;
import lta.base.fighter.entities.FighterEntity;
import lta.base.item.entities.ItemEntity;
import lta.data.NftMapperData;
import lta.data.UserData;
import lta.nft.data.LoginNFTUserData;
import lta.okwar.OkWarGlobalVar;
import lta.okwar.data.HeroData;
import lta.okwar.entities.mapstate.ShibaMapStateEntity;
import lta.okwar.entities.reward.ShibaRewardEntity;
import lta.smartfox.error.ErrorException;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;

public class OkWarEntity extends BaseUser {
    public OkWarEntity(SFSExtension extension) {
        super(extension);
    }

    @Override
    public void getUserInfo(UserData data) {
        super.getUserInfo(data);
        String walletAddress = ((LoginNFTUserData)data).walletAddress;
        updateHeroFighterData(walletAddress);
        try {
            //Fighter Entity initial
            FighterEntity fighterEntity = new FighterEntity(getExtension(), walletAddress);
            fighterEntity.sfsUser = sfsUser;
            map_Type_Entity.put("Fighters", fighterEntity);

            //Reward Entity initial
            ShibaRewardEntity rewardEntity = new ShibaRewardEntity(getExtension(), walletAddress);
            rewardEntity.sfsUser = sfsUser;
            map_Type_Entity.put("Reward", rewardEntity);

            //Mapstate Entity initial
            ShibaMapStateEntity mapStateEntity = new ShibaMapStateEntity(getExtension(), walletAddress);
            mapStateEntity.sfsUser = sfsUser;
            map_Type_Entity.put("MapState", mapStateEntity);

            //Item entity initial
            ItemEntity itemEntity = new ItemEntity(getExtension(),walletAddress);
            itemEntity.sfsUser = sfsUser;
            map_Type_Entity.put("Items",itemEntity);

        } catch (ErrorException e) {
            e.printStackTrace();
        }
    }

    void updateHeroFighterData(String walletAddress) {
        try {
            List<NftMapperData> nftMapperDataList = GlobalNftMapper.nftMapperModel.selectAllData(NftMapperData.class, new Document("walletAddress", walletAddress));
            if(nftMapperDataList == null || nftMapperDataList.size() == 0) return;

            List<ObjectId> heroIds = new ArrayList<>();
            List<HeroData> heroDataList = new ArrayList<>();

            for(NftMapperData nftMapperData : nftMapperDataList) {
                heroIds.add(nftMapperData.getGameId());
            }
            List<Bson> conditions = new ArrayList<>();
            Bson idCondition = match(in("_id", heroIds));
            conditions.add(idCondition);

            heroDataList = OkWarGlobalVar.heroModel.filterAllData(HeroData.class, conditions);
            for(HeroData hero : heroDataList) {
                if(!hero.ownId.equals(walletAddress)) {
                    hero.ownId = walletAddress;
                    OkWarGlobalVar.heroModel.updateData(hero);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
