package lta.okwar.entities.reward;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartfoxserver.v2.extensions.SFSExtension;
import lta.base.entity.EntityInformationGlobalVar;
import lta.base.reward.RewardGlobalVar;
import lta.base.reward.data.RewardData;
import lta.base.reward.data.RewardItemData;
import lta.base.reward.entities.RewardEntity;
import lta.okwar.data.reward.ShibaBaseReward;
import lta.smartfox.error.ErrorException;
import lta.utils.GachaUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShibaRewardEntity extends RewardEntity {
    private String ownId;
    public ShibaRewardEntity(SFSExtension extension, String ownId) throws ErrorException {
        super(extension, ownId, "Reward");
        this.ownId = ownId;
    }

    public List<RewardItemData> generateReward(String baseId) {
        try {
            ShibaBaseReward shibaBaseReward = (ShibaBaseReward) EntityInformationGlobalVar.map_Type_BaseEntityData.get("RewardConfig").get(baseId);
            RewardData rewardData = getData(baseId);
            if (rewardData == null) {
                rewardData = RewardGlobalVar.rewardModel.createReward(baseId, "RewardConfig",2, "campaign", ownId);
                //addNewData(rewardData);
            }

            List<RewardItemData> listRewardConfig = shibaBaseReward.data;

            Map<Integer, List<RewardItemData>> mapRewardItem = new HashMap<>();
            for (RewardItemData rewardItem : listRewardConfig) {
                if (!mapRewardItem.containsKey(rewardItem.tier)) {
                    mapRewardItem.put(rewardItem.tier, new ArrayList<>());
                }
                List<RewardItemData> listRewardTier = mapRewardItem.get(rewardItem.tier);
                listRewardTier.add(rewardItem);
                mapRewardItem.put(rewardItem.tier, listRewardTier);
            }

            List<RewardItemData> listReward = new ArrayList<>();
            for (int key : mapRewardItem.keySet()) {
                if (key > 1 && rewardData.level > 1) {
                    List<RewardItemData> temp = createRewardByLevel(mapRewardItem.get(key));
                    if (temp != null) listReward.addAll(temp);
                } else
                if (key == 1 && rewardData.level == 1){
                    List<RewardItemData> temp = createRewardByLevel(mapRewardItem.get(key));
                    if (temp != null) listReward.addAll(temp);
                    levelUpReward(baseId);
                    break;
                }
            }

            getExtension().trace("Reward: " + listReward.size());
            if(listReward != null && listReward.size() > 0)
                sendReward(listReward);
            return listReward;
        } catch (Exception e) {
            new ErrorException(getExtension(), sfsUser, "Cannot get reward");
            return null;
        }
    }

    List<RewardItemData> createRewardByLevel(List<RewardItemData> listRewardConfig) {
        List<RewardItemData> listReward = new ArrayList<>();
        List<RewardItemData> tempListReward = listRewardConfig;
        Stream<RewardItemData> listRate100 = tempListReward.stream().filter(t -> t.rate == 100.0);
        listReward = new ArrayList<RewardItemData>(listRate100.collect(Collectors.toList()));
        tempListReward = tempListReward.stream().filter(t -> t.rate != 100.0).collect(Collectors.toList());
        if (tempListReward.size() == 0) return listReward;
        double[] rate = tempListReward.stream().mapToDouble(g -> g.rate).toArray();
        int index = GachaUtils.randomDrop(rate);
        if (index == -1) return listReward;
        listReward.add(tempListReward.get(index));
        return listReward;
    }
//    public static void main(String[] args) throws JsonProcessingException {
//        for (int i = 0; i < 1000; i++) {
//            List<RewardItemData> listRewardConfig = new ArrayList<>();
//            listRewardConfig.add(new RewardItemData("up_01", 2, 100.0, "Material", 1));
//            listRewardConfig.add(new RewardItemData("unit_frag_01", 2, 100.0, "Material", 1));
//            listRewardConfig.add(new RewardItemData("up_01", 2, 70.0, "Material", 2));
//            listRewardConfig.add(new RewardItemData("up_01", 4, 30.0, "Material", 2));
//            listRewardConfig.add(new RewardItemData("chest_frag_01", 2, 30.0, "Material", 3));
//
//            Map<Integer, List<RewardItemData>> mapRewardItem = new HashMap<>();
//            for (RewardItemData rewardItem : listRewardConfig) {
//                if (!mapRewardItem.containsKey(rewardItem.tier)) {
//                    mapRewardItem.put(rewardItem.tier, new ArrayList<>());
//                }
//                List<RewardItemData> listRewardTier = mapRewardItem.get(rewardItem.tier);
//                listRewardTier.add(rewardItem);
//                mapRewardItem.put(rewardItem.tier, listRewardTier);
//            }
//
//            List<RewardItemData> listReward = new ArrayList<>();
//            for (int key : mapRewardItem.keySet()) {
//                if (key > 1) {
//                    List<RewardItemData> temp = createRewardByLevel(mapRewardItem.get(key));
//                    if (temp != null) listReward.addAll(temp);
//                } else {
//                    List<RewardItemData> temp = createRewardByLevel(mapRewardItem.get(key));
//                    if (temp != null) listReward.addAll(temp);
//                }
//            }
//
//            listReward.stream().forEach(x-> System.out.print("Reward: " + x.itemId + " -- "));
//            System.out.println();
//        }
//    }

}
