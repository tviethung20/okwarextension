package lta.okwar.model.mapstate;

import lta.base.entity.model.GameModel;
import lta.base.mapstate.data.MapStateData;
import lta.base.mapstate.model.MapStateModel;
import lta.okwar.data.mapstate.ShibaMapStateData;

public class ShibaMapStateModel extends MapStateModel<ShibaMapStateData> {
    public ShibaMapStateData createMapState(String baseId, String baseName, int level, String type, String ownId) throws Exception {
        ShibaMapStateData mapStateData = new ShibaMapStateData(baseId, baseName, level);
        mapStateData.type = type;
        mapStateData.ownId = ownId;
        insertData(mapStateData);
        return mapStateData;
    }

    @Override
    protected Class<ShibaMapStateData> getDataType() {
        return ShibaMapStateData.class;
    }
}
