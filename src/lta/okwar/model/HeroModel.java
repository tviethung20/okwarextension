package lta.okwar.model;

import lta.base.fighter.models.FighterModel;
import lta.fighter.data.FighterData;
import lta.okwar.data.HeroData;
import org.bson.Document;
import org.bson.types.ObjectId;

public class HeroModel extends FighterModel {
    public HeroData createHero(String baseName, String baseId, String ownerAddress) throws Exception {
        HeroData heroData = new HeroData(baseId, baseName, 1, ownerAddress);
        insertData(heroData);
        return heroData;
    }

    public HeroData getHeroById(ObjectId id) throws Exception {
        HeroData hero = (HeroData)this.selectData(new Document("_id", id), HeroData.class);
        return hero;
    }
}
