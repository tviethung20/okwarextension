package lta.okwar;

import lta.okwar.model.HeroModel;
import lta.okwar.model.mapstate.ShibaMapStateModel;

public class OkWarGlobalVar {
    public static volatile HeroModel heroModel;
    public static volatile ShibaMapStateModel mapStateModel;
}
