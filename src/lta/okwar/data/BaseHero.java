package lta.okwar.data;

import lta.base.entity.entities.BaseEntityInformation;
import org.bson.Document;

public class BaseHero extends BaseEntityInformation<BaseHeroData> {
    public String heroType;
    public String heroGen;
    public String heroRace;
    public String rarity;
    public double price;
    public int gender;

    public BaseHero() {
    }

    @Override
    public void initData(Document document) throws Exception {
        super.initData(document);
        heroType = document.getString("heroType");
        heroGen = document.getString("heroGen");
        heroRace = document.getString("heroRace");
        rarity = document.getString("rarity");
        price = document.getDouble("price");
        gender = document.getInteger("gender");
    }

    @Override
    protected Class<BaseHeroData> getDataType() {
        return BaseHeroData.class;
    }
}
