package lta.okwar.data;

import lta.fighter.data.FighterData;
import org.bson.Document;

public class HeroData extends FighterData {
    public HeroData() {
    }

    public HeroData(String baseId, String baseName, int level, String owner) throws Exception {
        super(baseId, baseName, level);
        this.ownId = owner;
        this.type = "Hero";
        this.listEntityName = "Hero";
    }

    @Override
    public void setDocument(Document document) throws Exception {
        super.setDocument(document);
    }

    @Override
    public Document getDocument() {
        return super.getDocument();
    }


}
