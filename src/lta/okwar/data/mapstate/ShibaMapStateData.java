package lta.okwar.data.mapstate;

import lta.base.entity.EntityInformationGlobalVar;
import lta.base.entity.data.BaseEntityInformationData;
import lta.base.entity.entities.BaseEntityInformation;
import lta.base.mapstate.data.MapStateData;
import lta.okwar.data.reward.ShibaBaseReward;
import org.bson.Document;

import java.util.HashMap;

public class ShibaMapStateData extends MapStateData {
    public int story;
    public int mission;
    public int stage;

    public ShibaMapStateData() {
    }

    public ShibaMapStateData(String baseId, String baseName, int level) throws Exception {
        super(baseId, baseName, level);
        getData();
    }

    protected void getData() {
        try {
            if (!EntityInformationGlobalVar.map_Type_BaseEntityData.containsKey(baseName))
                throw new Exception("Base name " + baseName + " is not exist");
            HashMap<String, BaseEntityInformation> baseItems = EntityInformationGlobalVar.map_Type_BaseEntityData.get(baseName);
            if (!baseItems.containsKey(baseId)) throw new Exception("baseId " + baseId + "is not exist");
            ShibaBaseReward baseEntityInformation = (ShibaBaseReward) baseItems.get(baseId);
            story = baseEntityInformation.story;
            mission = baseEntityInformation.mission;
            stage = baseEntityInformation.stage;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setDocument(Document document) throws Exception {
        super.setDocument(document);
        this.story = document.getInteger("story");
        this.mission = document.getInteger("mission");
        this.stage = document.getInteger("stage");
    }

    @Override
    public Document getDocument() {
        Document document = super.getDocument();
        document.append("story", story);
        document.append("mission", mission);
        document.append("stage", stage);
        return document;
    }
}
