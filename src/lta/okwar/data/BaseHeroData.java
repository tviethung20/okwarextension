package lta.okwar.data;

import lta.base.entity.data.BaseEntityInformationData;
import org.bson.Document;

public class BaseHeroData extends BaseEntityInformationData {
    public double attack;
    public double accuracy;
    public double agility;

    public BaseHeroData() {
    }

    @Override
    public Document getDocument() {
        Document document = super.getDocument();
        document.append("attack", attack);
        document.append("accuracy", accuracy);
        document.append("agility", agility);
        return document;
    }

    @Override
    public void setDocument(Document document) throws Exception {
        super.setDocument(document);
        attack = document.getDouble("attack");
        accuracy = document.getDouble("accuracy");
        agility = document.getDouble("agility");
    }
}
