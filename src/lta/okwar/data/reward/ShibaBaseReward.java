package lta.okwar.data.reward;

import lta.base.entity.entities.BaseEntityInformation;
import lta.base.reward.data.RewardItemData;
import org.bson.Document;

public class ShibaBaseReward extends BaseEntityInformation<RewardItemData> {
    public int story;
    public int mission;
    public int stage;

    @Override
    public void initData(Document document) throws Exception {
        super.initData(document);
        this.story = document.getInteger("story");
        this.mission = document.getInteger("mission");
        this.stage = document.getInteger("stage");
    }

    @Override
    protected Class<RewardItemData> getDataType() {
        return RewardItemData.class;
    }
}
